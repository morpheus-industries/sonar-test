package org.vadar.server.db.ejb.service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.slf4j.Logger;

@Singleton
@Startup
public class ServiceStartupBean {

    @Inject
    private Logger logger;

    @PostConstruct
    void onServiceStart() {
        logger.info("Starting service");
    }

    @PreDestroy
    void onServiceStop() {
        logger.info("Stopping service");
    }

}
