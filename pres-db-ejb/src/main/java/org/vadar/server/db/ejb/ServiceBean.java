
package org.vadar.server.db.ejb;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;

@Stateless
public class ServiceBean implements ServiceRemote, ServiceLocal {

    private static final String DATE_FORMAT = "dd/MM/yyyy";

    @Inject
    private Logger logger;

    public String resolveDate() {
        logger.debug("In EJB - resolving date");
        final DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        return df.format(Instant.now());
    }

}
