package org.vadar.presentation.server.db.rest.resources;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;

import java.util.concurrent.atomic.AtomicInteger;

/*
 * Rest Services 
 *  
 * Resources are served relative to the servlet path specified in the {@link ApplicationPath}
 * annotation.
 * 
 */
@Path("/resource")
@RequestScoped
public class CounterRestResource {

    @Inject
    private Logger logger;

    private AtomicInteger counter;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("increaseCount")
    public String sendMessage() {
        counter.incrementAndGet();
        this.logger.debug("counter incremented: {}", counter.get());
        return "ok";
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getCount")
    public String getStats() {
        return "Sent " + counter.get();
    }

}